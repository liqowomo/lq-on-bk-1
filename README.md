<h1 align="center"><code> 👢: lq-on-bk-1 </code></h1>
<h1 align="center"><i> Documentation site frameworks testing </i></h1>

----
1. [Huh ?](#huh-)
   1. [Docusaurus - from FagMeta](#docusaurus---from-fagmeta)
   2. [Nextra - Vercel Template](#nextra---vercel-template)
2. [Branches](#branches)

----

# Huh ? 

Testing the following frameworks 

## Docusaurus - from FagMeta

[`https://docusaurus.io/`](https://docusaurus.io/)
- Most commonly used  by commercial projects 

## Nextra - Vercel Template

[`https://nextra.site/`](https://nextra.site/)
- Modern framework 
- Minimalisitc 
- Had issues with rendering latex when using code sandbox 
  - Testing this issue here 


# Branches 

1. Each attempt will keep in a seperate branch 
2. Code for Docusaurus will be doXX eg: do1 , do2, 
3. Code for Nextra will be neXX eg: ne1, ne2  

N | ? 
|:--:|:--:|
ne1 | Beginner nextra Test